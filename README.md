# Обучение с подкреплением с использованием модели мира. ДЗ 4 (vo_HW)

<b>Задача</b>: реализация и анализ алгоритма Dyna на детерминированной и стохастической среде, основанной на среде из первого задания.

### Детерминированная среда

* Реализовать табличный алгоритм Dyna на основе среды из первого задания.
* Протестировать Dyna с различным количеством шагов планирования начиная с 0 (которое соответствует обычному Q-обучению).
* Сравнить результаты в виде графиков суммарного вознаграждения и времени обучения для каждого значения шагов планирования.

### Стохастическая среда:

* Модифицировать среду, чтобы она стала стохастической, удостоверившись, что нескольких состояний существует как минимум два состояния с ненулевой вероятностью перехода.
* Адаптировать алгоритм Dyna для учета стохастичности среды.
* Предоставить аналогичные графики, как и в первой части задания.


